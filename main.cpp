#include <iostream>
#include <fstream>
#include <cmath>


using namespace std;

typedef double dbl;

dbl f(dbl x) {
    return x * sin(x) + cos(x);
}

dbl solveByN(unsigned N,
             dbl a, dbl b,
             dbl (*f)(dbl)) {
    auto    A = (dbl)0.22222222,
            A1 = (dbl)1.02497166,
            A2 = (dbl)0.75280612;
    auto    t0 = (dbl)-1,
            t1 = (dbl)-0.28989794,
            t2 = (dbl)0.68989794;


    dbl I = 0;
    for (int i = 0; i < N; i++) {

        dbl ai = a + (b - a) / (N) * i,
            bi = a + (b - a) / (N) * (i + 1);


        dbl     x0 = (ai+bi) / 2 + (bi-ai) / 2 * t0,
                x1 = (ai+bi) / 2 + (bi-ai) / 2 * t1,
                x2 = (ai+bi) / 2 + (bi-ai) / 2 * t2;
        dbl Ii = (bi-ai) / 2 * (A * f(x0) +  A1 * f(x1) + A2 * f(x2));
        I += Ii;

    }
    return I;
}

pair<int, dbl> solveByRunge(double E,
                 dbl a, dbl b,
                 dbl (*f)(dbl)) {
    unsigned N = 2;
    dbl I1 = solveByN(N, a, b, f);
    dbl I2;
    do {
        I2 = I1;
        N *= 2;
        I1 = solveByN(N, a, b, f);
    } while ((I2-I1) / 15 > E);
    return {N, I1};
}


int main(int argv, char ** args) {
    ifstream in("/Users/mark/CLionProjects/KateMarkoff/in.txt");
    ofstream out("/Users/mark/CLionProjects/KateMarkoff/out.txt");



    switch (args[1][0]) {
        case 'N': {
            unsigned N;
            dbl a, b;
            in >> N >> a >> b;
            out << N << " " << solveByN(N, a, b, f) << endl;
        } break;
        case 'E': {
            dbl E;
            dbl a, b;
            in >> E >> a >> b;
            pair<int, dbl> r = solveByRunge(E, a, b, f);
            out << r.first << " "  << r.second << endl;
        } break;
        default:
            cout << "you must you 'N' or 'E' as a param" << endl;
    }



    return 0;
}